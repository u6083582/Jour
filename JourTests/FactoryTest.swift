//
//  FactoryTest.swift
//  JourTests
//
//  Created by Pandu Kerr on 21/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import XCTest
@testable import Jour

class FactoryTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    
    func test_makeOccurrence_ErrorThrownOnMakeOccurrenceWithoutEntry () {
        let f : FactoryProtocol = Factory ()
        
        XCTAssertThrowsError(try f.makeOccurrence(entryId: 6, date: Date ())) {
            (error) -> Void in XCTAssertEqual(error as? CreationError, CreationError.AttemptedToCreateOccurrenceForNonExistentEntry)
        }
    }
    
    func test_makeOccurrence_ThrowNoErrorOnOccurrenceWithEntry () {
        let f : FactoryProtocol = Factory ()
        let i : Int = f.makeEntry(name: "entry two", date: Date ())
        XCTAssertNoThrow(try f.makeOccurrence(entryId: i, date: Date.distantPast))
    }
    
    func test_makeOccurrence_ThrowsOnMakingOccurrenceOnSameDateAsDefaultOccurrence () {
        let f : FactoryProtocol = Factory ()
        let d : Date = Date()
        let i : Int  = f.makeEntry(name: "entry three", date: d)
        
        XCTAssertThrowsError(try f.makeOccurrence(entryId: i, date: d)) {
            (error) -> Void in XCTAssertEqual(error as? CreationError, CreationError.AttemptedToCreateOccurrenceOnDateOfExistingOccurrence)
        }
    }
        
    func test_init_RepositoryInstantiatedAtInit (){
        let f : FactoryProtocol = Factory ()
        var r : RepositoryProtocol = Repository ()
        XCTAssertNoThrow(r = f.getRepository())
    }
    
    
    
}
