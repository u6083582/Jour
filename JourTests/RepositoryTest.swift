//
//  RepositoryTest.swift
//  JourTests
//
//  Created by Pandu Kerr on 24/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import XCTest
@testable import Jour

class RepositoryTest: XCTestCase {
    
    override func setUp() { super.setUp()}
    
    override func tearDown() {super.tearDown()}
    
/* TODO:
     Make test for two dates are very close (same day) they should register as the same date.
 */
    
    // .doesPageExist()
    func test_doesPageExist_noneDoAtStart () {
        let f : FactoryProtocol = Factory ()
        let d : Date = Date()
        XCTAssertFalse(f.getRepository().doesPageExist (date : d))
    }
    
    func test_doesPageExist_itDoes () {
        let f : FactoryProtocol = Factory ()
        let d : Date = Date  ()
        let _ = try! f.makeEntry(name: "entry one", date: d)
        XCTAssertTrue(f.getRepository().doesPageExist(date: d))
    }
    
    
    // .doesOccurrenceExist()
    func test_doesOccurrenceExist_itDoes() {
        let f : FactoryProtocol = Factory ()
        let r : RepositoryProtocol = f.getRepository()
        
        let entryId      : Int = try! f.makeEntry(name: "testString", date: Date ())
        let occurrenceId : Int = try! f.makeOccurrence(entryId: entryId, date: Date.distantPast)
        
        XCTAssertTrue(r.doesOccurrenceExist(id: occurrenceId))
    }
    
    func test_doesOccurrenceExist_itDoesnt() {
        let f : FactoryProtocol = Factory ()
        let r : RepositoryProtocol = f.getRepository()
        
        XCTAssertFalse(r.doesOccurrenceExist(id: 60000))
    }
    
    
    // .addEntry()
    func test_addEntry_EntryIsRetrievable_NoThrow () {
        let f : FactoryProtocol    = Factory ()
        let r : RepositoryProtocol = f.getRepository()
        
        let entryId : Int =  try! f.makeEntry(name: "test_addEntry_addingEntryIncreasesDictionarySize"
            , date: Date ())
        XCTAssertNoThrow(try! r.getEntry(id: entryId))
    }
    
    func test_addEntry_EntryIsRetrievable_SameDate () {
        let f : FactoryProtocol    = Factory ()
        let r : RepositoryProtocol = f.getRepository()
        let s : String             = "TestingEntryName"
        
        let entryId : Int =  try! f.makeEntry(name: s, date: Date ())
        XCTAssertEqual(try! r.getEntry(id: entryId).readName() , s)
    }
    
    // .addOccurrence()
    func test_addOccurrence_uniqueOccurrenceId () {
        let f : FactoryProtocol = Factory ()
        
        let entryId : Int = try! f.makeEntry (name: "test_addOccurrence_uniqueOccurrenceId"
            , date: Date ())
        let _ : Int = try! f.makeOccurrence(entryId: entryId, date: Date ())
        
        XCTAssertNoThrow(try! f.makeOccurrence(entryId: entryId, date: Date.distantPast))
    }
    
            // Find a way to test this
    func test_addOccurrence_sameOccurrenceId () {
    }
    
    // .addEntry()
  
    

    
}
