//
//  Repository.swift
//  Jour
//
//  Created by Pandu Kerr on 20/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    func doesPageExist  (date  : Date)       -> Bool
    func addEntry       (entry : EntryProtocol) throws
    func getEntry       (id    : Int) throws -> EntryProtocol
    func doesEntryExist (id    : Int)        -> Bool
    func addOccurrence  (occurrence : OccurrenceProtocol) throws
    func doesOccurrenceExist (id: Int)       -> Bool
    func getOccurrence  (id   : Int)  throws -> OccurrenceProtocol
    func addPage        (page : Page) throws
    func getPage        (date : Date) throws -> Page
}


/*
  This class is responsible for checking if added objects are unique (using ID's).
  The dictionaries used to store the objects use the Entry, Occurrence and Pages superclasses,
  and shouldn't discriminate amongst their subclasses.
 */

class Repository : RepositoryProtocol{

    private var entries     = [Int  : EntryProtocol]      ()
    private var occurrences = [Int  : OccurrenceProtocol] ()
    private var pages       = [Date : Page]               ()
    private var stdCalendar = Calendar (identifier: Calendar.Identifier.gregorian)
    
    func addOccurrence (occurrence: OccurrenceProtocol) throws {
         if (!doesOccurrenceExist (id: occurrence.readId())) {
            throw RetrievalError.coudlntFindOccurrence
         } else {
            occurrences [occurrence.readId()] = occurrence
        }
    }
    
    func doesOccurrenceExist (id: Int) -> Bool {
        return occurrences [id] != nil ? false : true
    }
    
    //throws if another entry with the same ID exists
    func addEntry (entry: EntryProtocol) throws{
        if doesEntryExist(id: entry.readId()) {
            throw AddingToCollectionError.addEntryError_FoundSameId
        } else {
            entries [entry.readId()] = entry
        }
    }
    
    // throws error if the occurrence doesn't exist
    func getEntry(id: Int) throws -> EntryProtocol {
        if let entry = entries [id] {
            return entry
        } else {
            throw RetrievalError.couldntFindEntry
        }
    }
    
    func doesEntryExist(id: Int) -> Bool {
        return entries [id] == nil ? false : true
    }
    
    func doesPageExist(date: Date) -> Bool {
        let standardisedDate = stdCalendar.startOfDay(for: date)
        return pages [standardisedDate] == nil ? false : true
    }
    
    func getOccurrence(id: Int) throws -> OccurrenceProtocol {
        if let occurrence = occurrences [id] {
            return occurrence
        } else {
            throw RetrievalError.coudlntFindOccurrence
        }
    }
    
    func addPage(page: Page) throws {
        if !doesPageExist(date: page.getDate()){
            pages [page.getDate()] = page
        } else {
            throw AddingToCollectionError.addPageError_FoundSameId
        }
    }

    
    func getPage(date: Date) throws -> Page {
        let standardisedDate = self.standardiseDate(date: date)
        if doesPageExist(date : standardisedDate) {
            return pages [standardisedDate]!
        } else
        {
            throw RetrievalError.couldntFindPage
        }
    }
    
    private func standardiseDate (date : Date) -> Date {
        return stdCalendar.startOfDay(for: date)
    }
    
}
