//
//  DomainController.swift
//  Jour
//
//  Created by Pandu Kerr on 20/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

public class DomainController{
    
    let factory    : FactoryProtocol
    let repository : RepositoryProtocol
    
    public init () {
        self.factory = Factory ()
        repository   = self.factory.getRepository ()
        
    }
    
}
