//
//  Entity.swift
//  Jour
//
//  Created by Pandu Kerr on 21/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

protocol EntityProtocol {
    func readId () -> Int
}

public class Entity : EntityProtocol {
 
    private let id : Int
    
    init (id : Int) {
        self.id = id
    }
    
    func readId () -> Int {
        let b : Int = id
        return b
    }
    
}
