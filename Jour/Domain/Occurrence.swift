//
//  Occurrence.swift
//  Jour
//
//  Created by Pandu Kerr on 20/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

protocol OccurrenceProtocol : EntityProtocol {
}

class Occurrence : Entity, OccurrenceProtocol {
   
    private let date    : Date
    private let entryID : Int
    private var content : OccurrenceContent = EmptyOccurrenceContentObject ()
    
    // TODO: check to see if entryID exists.
    //    * In subclasses, check that entryId corresponds to an entry of a corresponding type
    init (id : Int, date : Date, entryId : Int) {
        self.date    = date
        self.entryID = entryId
        super.init(id : id)
        
    }
    
    
}
