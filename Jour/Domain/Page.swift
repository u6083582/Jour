//
//  Page.swift
//  Jour
//
//  Created by Pandu Kerr on 20/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

class Page {

    private let date : Date
    private var occurrenceIds = Set <Int>()

    
    init (date : Date, calendar : Calendar){
        self.date = date
    }
    

    func addOccurrence (id : Int) throws {
        if occurrenceIds.contains(id) {
            throw AddingToCollectionError.addOccurrenceError_FoundSameId
        } else {
            occurrenceIds.insert(id)
        }
    }
    
    func isTheSameDate (other : Date, cal : Calendar) -> Bool {
        return cal.isDate(self.date, inSameDayAs: other)
    }
    
    func getDate () -> Date {
        return date
    }
    
}

