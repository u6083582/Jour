//
//  Entry.swift
//  Jour
//
//  Created by Pandu Kerr on 20/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation


protocol EntryProtocol : EntityProtocol {
    func addOccurrence (id : Int) throws
    func readName () -> String
}

// Check if new occurrence date clashes
class Entry : Entity, EntryProtocol{

    private let name      : String
    private var occurrenceIds = Set <Int>()
    private let factory   : Factory
    
    init (id: Int, name: String, occurrenceId: Int, firstOccurrenceDate : Date, factory : Factory) {
        self.name = name
        self.factory = factory
        super.init(id : id)
        try! addOccurrence(id: occurrenceId)
        
        let _ = try! factory.makeOccurrence(entryId: self.readId(), date: firstOccurrenceDate)
    }

    //throws addOccurrenceError_FoundSameId if the occurrence ID is already contained
    func addOccurrence(id : Int) throws{
        if occurrenceIds.contains(id) {
            throw AddingToCollectionError.addOccurrenceError_FoundSameId
        } else {
            occurrenceIds.insert(id)
        }
    }
    
    func readName() -> String {
        return name
    }
    
    
}


