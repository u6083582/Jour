//
//  DomainErrors.swift
//  Jour
//
//  Created by Pandu Kerr on 21/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

enum CreationError : Error {
    case AttemptedToCreateOccurrenceForNonExistentEntry
    case AttemptedToCreateOccurrenceOnDateOfExistingOccurrence
}

enum RetrievalError : Error {
    case couldntFindEntry
    case coudlntFindOccurrence
    case couldntFindPage
}

enum AddingToCollectionError : Error {
    case addEntryError_FoundSameId
    case addOccurrenceError_FoundSameId
    case addPageError_FoundSameId
}

enum ConstructionError : Error {
    case incompleteFunctionCalled
}

enum IdError : Error {
    case MaxIdReached
}
