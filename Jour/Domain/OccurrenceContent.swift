//
//  OccurrenceContent.swift
//  Jour
//
//  Created by Pandu Kerr on 16/12/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

protocol OccurrenceContent {}

public class EmptyOccurrenceContentObject : OccurrenceContent{}

