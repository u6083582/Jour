
//
//  Factory.swift
//  Jour
//
//  Created by Pandu Kerr on 20/11/17.
//  Copyright © 2017 Northbourne. All rights reserved.
//

import Foundation

protocol FactoryProtocol {
    func getRepository  () -> RepositoryProtocol
    func makeEntry      (name : String, date : Date) throws -> Int
    func makeOccurrence (entryId : Int, date : Date) throws -> Int
    func makePage       (date : Date)
}

/*
 
 The Factory's make functions have the following resposnisibilites: to Create a new instance and request that the instance is added to the repository. The make...() functions return the new instance's ID.
 */

class Factory : FactoryProtocol{
    
    let repository : Repository
    private var nextId : Int = 1
    
    init (){
        repository = Repository ()
    }
    
    func getRepository() -> RepositoryProtocol {
        return repository
    }
    
    
    func makeEntry(name : String, date : Date) throws -> Int{
        let entryId         : Int = try! self.makeId()
        let fstOccurrenceID : Int = try! self.makeId()
        //MAKING ENTRY
        let e : EntryProtocol = Entry (id: entryId, name: name, occurrenceId: fstOccurrenceID
            , firstOccurrenceDate: date, factory: self)
        
        //ADDING TO REPO
        try! repository.addEntry(entry: e)
        
        return entryId
    }
    
    
    func makeOccurrence(entryId: Int, date: Date) throws -> Int {
        let occurrenceId : Int     = try! self.makeId()
        let o : OccurrenceProtocol = Occurrence (id: occurrenceId, date : date, entryId : entryId)
        //ADDING TO REPO
        try! repository.addOccurrence(occurrence: o)
        
        //ADDING TO ENTRY
        let entry = try! repository.getEntry(id: entryId)
        try! entry.addOccurrence(id: occurrenceId)
        
        //ADDING TO PAGE
        var page : Page
        if (!repository.doesPageExist(date: date)) {
            self.makePage (date : date)
        }
        page = try! repository.getPage(date: date)
        try! page.addOccurrence (id : occurrenceId)
       
        return occurrenceId
    }
    
    
    func makePage(date: Date) {
        let c : Calendar  = Calendar (identifier: Calendar.Identifier.gregorian)
        let normalisedDate = c.startOfDay(for: date)
        let p : Page      = Page (date: normalisedDate,calendar: c)
        try! repository.addPage(page: p)
    }
    
    
    private func makeId ()throws -> Int {
        let id = nextId
        nextId += 1
        if nextId == Int.max {throw IdError.MaxIdReached}
        return id
    }
}
